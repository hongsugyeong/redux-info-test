<img src="/public/img/Nextjs.png">

# 정보 입력 후 보여주기

## 프로젝트 간단 설명
Open [http://localhost:3000](http://localhost:3000)

내 정보를 입력해보세요 <br>
그러면 내 정보가 나타납니다!

### 프로젝트 실행 화면

#### 첫번째 화면
<img src="/public/img/redux-info-1.png">
이름을 입력한 다음 귀요미는 누구일지 확인해보세요 <br>
Component를 통해 입력받은 값이 올바르게 들어오는지 확인할 수 있습니다

#### 두번째 화면
<img src="/public/img/redux-info-2.png">
입력하신 이름이 나오는 걸 확인해보세요 <br>
이제 나이를 입력해봅시다 <br>
Component를 통해 값이 들어왔는지 확인하고, 입력값이 올바르게 들어오는지 확인할 수 있습니다

#### 세번째 화면
<img src="/public/img/redux-info-3.png">
나이를 입력한 다음 내 정보를 확인해보세요 <br>
위와 마찬가지로 Component를 통해 확인할 수 있습니다


### 사용한 기술
- Redux
- Redux Toolkit
- Redux persist
- React Developer Tools