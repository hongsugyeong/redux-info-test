'use client'

import {useDispatch, useSelector} from "react-redux";
import Link from "next/link";
import {useState} from "react";
import {patchMemberAge} from "@/lib/features/member/memberSlice";

export default function Geni() {
    const { memberName } = useSelector(state => state.member)
    const [memberAge, setMemberAge] = useState('')
    const dispatch = useDispatch()

    const handleSubmit = () => {
        dispatch(patchMemberAge(Number(memberAge)))
    }

    return (
        <main className="flex min-h-screen flex-col items-center justify-between p-24">
            <div>{memberName} 당신은 최강 귀요미</div>
            <input type="text" value={memberAge} onChange={e => setMemberAge(e.target.value)}/>
            <button onClick={handleSubmit}><Link href={'/result'}>나이 입력 ㄱㄱ</Link></button>
        </main>
    )
}