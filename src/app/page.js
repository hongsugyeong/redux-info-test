'use client'
import { useState } from "react";
import { useDispatch } from "react-redux";
import { patchMemberName } from '@/lib/features/member/memberSlice'
import Link from "next/link";

export default function Home() {
  const [memberName, setMemberName] = useState('')
  const dispatch = useDispatch()

  const handleSaveName = () => {
      dispatch(patchMemberName(memberName))
      // dispatch를 통해 patchMemberName를 가져오고 memberName 넣어주기
  }

  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <input type="text" value={ memberName } onChange={ (e) => setMemberName(e.target.value) } />
      {/* 두번 이상 사용할 때는 handler 만들기, 한번만 사용할 때는 바로 사용하기 */}
      <button onClick={handleSaveName}><Link href="/geni">귀요미는 누구일까?</Link></button>

    </main>
  );
}
