'use client'

import { useSelector } from "react-redux";

export default function Result() {
    const { memberName, memberAge } = useSelector(state => state.member)

    return (
        <main className="flex min-h-screen flex-col items-center justify-between p-24">
                최강 귀요미 : { memberName }<br/>
                최강 나이 : { memberAge }
        </main>
    )
}