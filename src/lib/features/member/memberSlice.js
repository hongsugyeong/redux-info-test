import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    memberName: '',
    memberAge: 0
}

const memberSlice = createSlice({
    name: 'member',
    initialState,
    reducers: {
        patchMemberName: (state, action) => {
            // 부분 수정: patch, 전체 수정: put
            state.memberName = action.payload
            // payload에 key 값을 name으로 해서 넣어주기
            // payload를 통해 받은 값 넣어주기 => initialState 접근 가능
        },
        patchMemberAge: (state, action) => {
            state.memberAge = action.payload
        }
    }
})

export const { patchMemberName, patchMemberAge } = memberSlice.actions
export default memberSlice.reducer